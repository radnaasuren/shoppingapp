import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native'
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, Dimensions } from 'react-native'
import { Input } from '../components/textInput';
// import { SharedElement } from 'react-native-gesture-handler'
const { height, width } = Dimensions.get("window")
import { useCol, useColSearch } from '../hooks/useFirebase'

const Categories: React.FC<any> = ({ color, category, image }: any) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity style={{ padding: 10 }} 
        // onPress={() => navigation.navigate('category', { category: category })}
        >
            <View style={[styles.categorymain, { backgroundColor: color }]}>
                <Image source={{ uri: image }} />
                <Text>{category}</Text>
            </View>
        </TouchableOpacity>
    )
}

const RenderList = ({ categories }: any) => {
    return (
        <FlatList
            data={categories}
            numColumns={2}
            horizontal={false}
            keyExtractor={(item) => item.category}
            renderItem={({ item, index }) => (<Categories category={item.category} color={item.color} image={item.image} />)}
        />
    )
}

export const Explore: React.FC<any> = (props) => {
    const { collection: categories, loading } = useCol('categories');
    console.log('dadada =====>', categories)
    
    if (loading) {
        return null;
    }

    return (
        <View style={styles.container}>
            {/* <Input /> */}
            {categories && <RenderList categories={categories} />}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    categorymain: {
        width: 500,
        height: 200,
        borderRadius: 10
    }
})