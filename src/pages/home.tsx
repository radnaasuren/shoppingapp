import React from 'react'
import { ScrollView, StyleSheet, View, Text } from 'react-native'
import { ProductCard } from '../components/productCard'
import { Input } from '../components/textInput'
import styled from 'styled-components/native'


export const Home = () => {

     const CardTop = styled.View`
          flex-direction: row;
          align-items: center;
          padding-left: 25px;
          padding-right: 25px;
          justify-content: space-between;
          display: flex
     `

     const Card = styled.View`
          padding-top: 30px
     `

     const Div = styled.View`
          justify-content: center;
          align-items: center;
          padding-top: 25px;
          display: flex;
     `

     return (<>
          <ScrollView>
               <Div>
                    <Input />
               </Div>
               <Card>
                    <CardTop>
                         <Text style={{ fontSize: 24 }}>Exclusive Offer</Text>
                         <Text style={{ color: '#53B175', fontSize: 16 }}>See all</Text>
                    </CardTop>
                    <View style={{ paddingLeft: 25, paddingTop: 20 }}>
                         <ProductCard type='Priceg' price={4.99} name='Banana' amount='7pcs' photoUrl='https://firebasestorage.googleapis.com/v0/b/shopping-app-66692.appspot.com/o/pngfuel%201.png?alt=media&token=db5c926c-ef7f-43cd-adbb-cc0ece48d018' />
                    </View>
               </Card>
               <Card>
                    <CardTop>
                         <Text style={{ fontSize: 24 }}>Best Selling</Text>
                         <Text style={{ color: '#53B175', fontSize: 16 }}>See all</Text>
                    </CardTop>
                    <View style={{ paddingLeft: 25, paddingTop: 20 }}>
                         <ProductCard type='Priceg' price={4.99} name='Banana' amount='7pcs' photoUrl='https://firebasestorage.googleapis.com/v0/b/shopping-app-66692.appspot.com/o/pngfuel%201.png?alt=media&token=db5c926c-ef7f-43cd-adbb-cc0ece48d018' />
                    </View>
               </Card>
               <Card>
                    <CardTop>
                         <Text style={{ fontSize: 24 }}>Groceries</Text>
                         <Text style={{ color: '#53B175', fontSize: 16 }}>See all</Text>
                    </CardTop>
                    <View style={{ paddingLeft: 25, paddingTop: 20 }}>
                         <ProductCard type='Priceg' price={4.99} name='Banana' amount='7pcs' photoUrl='https://firebasestorage.googleapis.com/v0/b/shopping-app-66692.appspot.com/o/pngfuel%201.png?alt=media&token=db5c926c-ef7f-43cd-adbb-cc0ece48d018' />
                    </View>
               </Card>
          </ScrollView>
     </>)
}