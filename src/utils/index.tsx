import _ from 'lodash';
import React from 'react';

export const fibonacci: any = _.memoize(function (n = 0) {
    return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
});