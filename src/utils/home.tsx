import React from 'react'
import { SafeAreaView, Text } from 'react-native'
import { Stack } from '../components/stack'

const Home = () => {

    return (<>
    <SafeAreaView>
        <Stack backgroundColor={'red'}>
            <Text>
                red text
            </Text>
            <Text>
                green text
            </Text>
        </Stack>
        </SafeAreaView>
    </>)
}

export default Home; 