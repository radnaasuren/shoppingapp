import firestore from '@react-native-firebase/firestore';
import {useEffect, useState} from 'react';

export const useDoc: any = (path: string) => {
  const [doc, setDoc] = useState(null);

  useEffect(() => {
    firestore()
      .doc(path)
      .onSnapshot((res: any) => setDoc(res.data()));
  }, [path]);

  const updateDoc = (data: any) => {
    firestore()
      .doc(path)
      .set(
        {
          ...data,
        },
        {merge: true},
      );
  };

  const deleteDoc = () => firestore().doc(path).delete();

  return {doc, updateDoc, deleteDoc};
};

export const useCol = (path: any) => {
  const [collection, setCollection] = useState<any>([]);
  const [loading, setLoading] = useState(false)

  const createDoc = (data: any) => {
    firestore().collection(path).add(data);
  };

  useEffect(() => {
    setLoading(true);
    if (path === '') {
      firestore()
        .collection('users')
        .get()
        .then((res) => {
          const array: any = res.docs.map((cur) => {
            return cur.data();
          });
          setCollection(array);
        });
    } else {
      firestore()
        .collection(path)
        .onSnapshot((res) => {
          const array: any = res.docs.map((cur) => {
            return cur.data();
          });
          setCollection(array);
        });
    }
    setLoading(false)
  }, [path]);
  return {collection, createDoc, loading};
};

export const useColSearch = (path: any, field: any, value: any) => {
  const [collection, setCollection] = useState([]);
  useEffect(() => {
    firestore()
      .collection(path)
      .get()
      .then((querySnapshot: any) => {
        const events = querySnapshot.docs.map((cur: any) => cur.data());
        setCollection(
          events.filter(
            (cur: any) =>
              cur[field] &&
              cur[field].toLowerCase().search(value.toLowerCase()) !== -1 &&
              value !== '',
          ),
        );
      });
  }, [path, field, value]);

  return {collection};
};