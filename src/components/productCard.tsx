import React from 'react'
import { Image, Text, View } from 'react-native'
import styled from 'styled-components/native'
import { PlusIcon } from '../svg'
import { Button } from './button'

type Card = {
   photoUrl: string,
   amount: string,
   type: 'Price' | 'Priceg',
   price: number,
   name: string,
}

export const ProductCard: React.FC<Card> = ({ name, photoUrl, amount, type, price }) => {
   const Card = styled.View`
      border-radius: 18px;
      border: 1px #E2E2E2 solid;
      width: 173px;
      height: 248px;
      padding-bottom: 15px;
   `

   const Img = styled.View`
      display: flex;
      justify-content: center;
      align-items: center;
      height: 129px
   `

   const SubText = styled.Text`
      font-size: 14px;
      color: #7C7C7C;
      padding-bottom: 20px;
      line-height: 18px
   `

   const Name = styled.Text`
      font-size: 16px;
      padding-bottom: 5px;
      line-height: 18px;
      font-weight: 800
   `

   const Bottom = styled.View`
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      padding-right: 14px;
      font-weight: 800
   `

   return (<>
      <Card>
         <Img>
            <Image resizeMode='contain' style={{ height: 90, width: 173, overflow: 'visible' }} source={{ uri: photoUrl }} />
         </Img>
         <View style={{ paddingLeft: 15 }}>
            <Name>{name}</Name>
            <SubText>{amount}, {type}</SubText>
            <Bottom>
               <Text style={{ fontSize: 18 }}>${price}</Text>
               <Button size='small' onPress={() => { console.log('Purchase') }}>
                  <PlusIcon />
               </Button>
            </Bottom>
         </View>
      </Card>
   </>)
}
