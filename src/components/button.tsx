import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { CheckIcon, LeftArrowIcon } from '../svg/icons'

const { width } = Dimensions.get('screen')

type Button = {
   size?: 'large' | 'small',
   type?: 'primary' | 'secondary' | 'fab' | 'cart',
   onPress: any,
   children?: any,
   align?: 'center' | 'evenly'
}

export const Button: React.FC<Button> = ({ type, size, onPress, align, children }) => {
   const styles = StyleSheet.create({
      button: {
         width: size === 'small' && type !== 'fab' ? 46 : type === 'fab' ? 67 : width - 50,
         height: size === 'small' && type !== 'fab' ? 46 : 67,
         borderRadius: type === 'fab' ? 40 : 19,
         backgroundColor: type === 'secondary' ? 'white' : '#53B175',
         borderColor: '#E2E2E2',
         borderWidth: type === 'secondary' ? 1 : 0,
         justifyContent: align === 'center' ? 'center' : 'space-evenly',
         alignItems: 'center',
         flexDirection: 'row',
      },
      cart: {
         width: width - 50,
         justifyContent: 'space-between',
         alignItems: 'center',
         backgroundColor: '#6FAE79',
         flexDirection: 'row',
         height: 67,
         borderRadius: 19,
         padding: 15,
      }
   })

   return (<>
      <TouchableOpacity style={type !== 'cart' ? styles.button : styles.cart} onPress={onPress}>
         {type !== 'cart' ? children : <>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
               <View style={{backgroundColor: '#85ba8d', height: 30, width: 30, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                  <CheckIcon />
               </View>
               <Text style={{ fontSize: 18, color: 'white' }}> Add to Cart</Text>
            </View>
            <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}>
               <Text style={{ fontSize: 14, color: 'white' }}>Open Cart  </Text>
               <LeftArrowIcon />
            </TouchableOpacity>
         </>}
      </TouchableOpacity>
   </>)
}