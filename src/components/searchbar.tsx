import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableHighlight
} from 'react-native';
import { Path, Svg } from 'react-native-svg'
import { SearchIcon } from '../svg/index'
export const Searchbar = () => {
    const [value, setValue] = useState('');
    const onChange = (text: any) => {
        console.log(text);
        setValue(text)
    };
    return (
        <>
            <View style={[styles.searchbackground]}>
                <View style={[styles.absolute, styles.row]}>
                    <View style={{ width: 35, height: 51 }}>
                        <SearchIcon />
                    </View>
                    <TextInput style={[styles.input]}
                        placeholder='Search Store'
                        onChangeText={onChange}
                    >
                    </TextInput>
                </View>
            </View>
        </>
    )
}
const styles = StyleSheet.create({
    searchbackground: {
        width: 364,
        height: 51,
        borderRadius: 15,
        backgroundColor: '#F2F3F2',
        justifyContent: 'center'
    },
    input: {
        fontSize: 14,
        color: '#7C7C7C',
    },
    absolute: {
        position: 'absolute',
    },
    row: {
        flexDirection: 'row'
    },
});
