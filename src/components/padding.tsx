import React from 'react';
import { StyleSheet, View } from 'react-native';
import { fibonacci } from '../utils'

export const Padding = ({
    children,
    top,
    bottom,
    left,
    right,
    debugColor,
}: {
    top?: any;
    bottom?: any;
    left?: any;
    right?: any;
    debugColor?: any;
    children: any;
}) => {
  const baseSpace = 5;
    const style = StyleSheet.create({
        container: {
            paddingTop: fibonacci(top) * baseSpace || 0,
            paddingBottom: fibonacci(bottom) * baseSpace || 0,
            paddingLeft: fibonacci(left) * baseSpace || 0,
            paddingRight: fibonacci(right) * baseSpace || 0,
            backgroundColor: debugColor,
        },
    });

    return <View style={style.container}>{children}</View>;
};