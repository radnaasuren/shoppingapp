import React, { useState } from 'react';
import { TextInput, StyleSheet, View, Dimensions } from 'react-native';
import { SearchIcon } from './icons';

const { width } = Dimensions.get('screen')

export const Input = () => {
    const [value, onChangeText] = useState('');

    return (
        <View style={styles.container}>
            <SearchIcon />
            <TextInput
                style={styles.input}
                placeholder='Search Store'
                placeholderTextColor='#7C7C7C'
                onChangeText={text => onChangeText(text)}
                value={value}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
        height: 52,
        width: width - 89,
        backgroundColor: '#F2F3F2',
        borderRadius: 15,
        paddingLeft: 0,
        marginLeft: 10
    },
    container: {
        height: 52,
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F2F3F2',
        borderRadius: 15,
    }
})