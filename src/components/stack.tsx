import React from 'react'
import { StyleSheet, View } from 'react-native'
import { fibonacci } from '../utils';

const Stack: React.FC<any> = ({
    children,
    height,
    width,
    backgroundColor,
}: {
    children?: any;
    width?: any;
    height?: any;
    color?: any;
    backgroundColor?: any;
}) => {
    return (
        <View
            style={{
                flexDirection: 'column',
                backgroundColor,
                height,
                width,
            }}>
            {React.Children.toArray(children).map((child, index) => {
                if (index == 0) {
                    return <View>{child}</View>;
                }
                return (
                    <View style={{
                        marginTop: fibonacci(2),
                        flexGrow: 2,
                    }}>
                        {child}
                    </View>
                );
            })
            }
        </View>
    )
}

export { Stack }